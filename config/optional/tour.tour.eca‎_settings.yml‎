langcode: en
status: true
dependencies:
  module:
    - eca_ui
id: eca_settings
label: 'ECA Settings'
module: eca_tour
routes:
  -
    route_name: eca.settings
tips:
  general-settings:
    id: general-settings
    plugin: text
    label: 'Settings'
    weight: -100
    position: auto
    selector: null
    body: 'On this page you are able to configure various settings for ECA.'
  log-level:
    id: log-level
    plugin: text
    label: 'Log level'
    weight: -99
    position: right
    selector: 'select#edit-log-level'
    body: 'Here you set the severity level from which events are being logged in ECA.'
  documentation-domain:
    id: documentation-domain
    plugin: text
    label: 'Documentation domain'
    weight: -98
    position: right
    selector: 'input#edit-documentation-domain'
    body: 'Here you define the domain the open documenation links in the modeler interface point to. You are able to replace the default domain with one of a self-hosted version. If the field is left empty, no open documentation links are shown.'
  execute-models-with-user:
    id: execute-models-with-usermap
    plugin: text
    label: 'Model execution'
    weight: -97
    position: auto
    selector: 'input#edit-user'
    body: 'The default behavior for this field is that the model is executed in the current user account with its permissions applied. The current user could be overridden by entering a user ID, username or UUID. The user context can still be overridden within the model.'
  dependency-calculation:
    id: dependency-calculation
    plugin: text
    label: 'Dependency calculation'
    weight: -96
    position: top
    selector: 'details#edit-dependency-calculation'
    body: 'Here you are able to define which dependencies are validated when a model is saved.'
  entity-bundle-config:
    id: entity-bundle-config
    plugin: text
    label: 'Entity bundle configurations'
    weight: -95
    position: right
    selector: div.form-item--dependency-calculation-bundle
    body: 'The setting adds entity bundle configurations that are used in a model that also exist for the site as dependencies.'
  field-storage-configurations:
    id: field-storage-configurations
    plugin: text
    label: 'Field storage configurations'
    weight: -94
    position: right
    selector: div.form-item--dependency-calculation-field-storage
    body: 'The setting adds field storage configurations that are used in a model that also exist for the site as dependencies.'
  field-configuration-per-bundle:
    id: field-configuration-per-bundle
    plugin: text
    label: 'Field configuration per bundle'
    weight: -93
    position: right
    selector: div.form-item--dependency-calculation-field-config
    body: 'The setting adds field configurations per bundle that are used in a model that also exist for the site as dependencies.'
  newly-added-field-configuration-per-bundle:
    id: newly-added-field-configuration-per-bundle
    plugin: text
    label: 'Newly added field configuration per bundle'
    weight: -92
    position: right
    selector: div.form-item--dependency-calculation-new-field-config
    body: 'The setting adds newly added field configurations per bundle that are used in a model that also exist for the site as dependencies.'
